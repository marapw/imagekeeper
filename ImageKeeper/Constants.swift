//
//  Constants.swift
//  ImageKeeper
//
//  Created by Марина Поворотная on 6.05.21.
//

import Foundation

public enum UserIDKeys: String {
     case password = "password"
     case imagesArray = "images array"
}
