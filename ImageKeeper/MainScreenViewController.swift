//
//  MainScreenViewController.swift
//  ImageKeeper
//
//  Created by Марина Поворотная on 6.05.21.
//

import UIKit

class MainScreenViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    //MARK: var/lets
    
    var imagesArray: [Image] = []
    
    //MARK: Lifecycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.downloadSavedInfo()
        
    }
    
    //MARK: IBActions
    @IBAction func addFotoButtonPressed(_ sender: UIButton) {
        self.showPickerAlert()
        
    }
    
    @IBAction func goToLibraryButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    //MARK: Flow functions
    func downloadSavedInfo() {
        guard let savedImages = UserDefaults.standard.value([Image].self, forKey: UserIDKeys.imagesArray.rawValue) else {
            return
        }
        self.imagesArray = savedImages
    }
    
    func showPickerAlert() {
        let alert = UIAlertController(title: "Choose image source", message: nil, preferredStyle: .actionSheet)
        let library = UIAlertAction(title: "Photo library", style: .default) { (_) in
            self.showImagePicker(sourceType: .photoLibrary)
        }
        let camera = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.showImagePicker(sourceType: .camera)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(library)
        alert.addAction(camera)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showImagePicker(sourceType: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = sourceType
        imagePicker.modalPresentationStyle = .overCurrentContext
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func saveImage(image: UIImage) -> String? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        
        let fileName = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let error {
                print("couldn't remove file at path", error)
            }
            
        }
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
    }
    
    func loadImage(fileName: String) -> UIImage? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageUrl = documentsDirectory.appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        return nil
    }
}

//MARK: Extensions

extension MainScreenViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            if let imageName = self.saveImage(image: image) {
                let image = Image(name: imageName, isLiked: false, comment: "")
                self.imagesArray.append(image)
                UserDefaults.standard.set(encodable: self.imagesArray, forKey: UserIDKeys.imagesArray.rawValue)
                self.collectionView.reloadData()
            }
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let imageName = self.saveImage(image: image) {
                let image = Image(name: imageName, isLiked: false, comment: "")
                self.imagesArray.append(image)
                UserDefaults.standard.set(encodable: self.imagesArray, forKey: UserIDKeys.imagesArray.rawValue)
                self.collectionView.reloadData()
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
extension MainScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configure(with: imagesArray[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (collectionView.frame.width - 20)/2
        return CGSize(width: side, height: side)
    }
}
