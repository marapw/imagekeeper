//
//  Image.swift
//  ImageKeeper
//
//  Created by Марина Поворотная on 17.05.21.
//

import Foundation

class Image: NSObject, Codable {
    var name: String
    var isLiked: Bool
    var comment: String
    
init(name: String, isLiked: Bool, comment: String) {
    self.name = name
    self.isLiked = isLiked
    self.comment = comment
    }
public enum CodingKeys: String, CodingKey {
            case name, isLiked, comment
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)

            try container.encode(self.name, forKey: .name)
            try container.encode(self.isLiked, forKey: .isLiked)
            try container.encode(self.comment, forKey: .comment)
        }
        
        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            self.name = try container.decode(String.self, forKey: .name)
            self.isLiked = try container.decode(Bool.self, forKey: .isLiked)
            self.comment = try container.decode(String.self, forKey: .comment)
        }
}
