//
//  GalleryViewController.swift
//  ImageKeeper
//
//  Created by Марина Поворотная on 6.05.21.
//

import UIKit

class GalleryViewController: UIViewController {
    //MARK:IBOutlets
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var commentTF: UITextField!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    //MARK: var/lets
    var imagesArray: [Image] = []
    var currentImageIndex = 0
    
    //MARK: Lificycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.downloadSavedInfo()
        self.registerForKeyboardNotifications()
        print(imagesArray)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setCommentTF()
        self.backgroundImageView.image = self.loadImage(fileName: imagesArray[currentImageIndex].name)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setUI()
        self.likeButton.isSelected = imagesArray[currentImageIndex].isLiked
        self.commentTF.text = imagesArray[currentImageIndex].comment
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }

    //MARK: IBActions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func commentInputted(_ sender: UITextField) {
        imagesArray[self.currentImageIndex].comment = self.commentTF.text ?? ""
        UserDefaults.standard.set(encodable: imagesArray, forKey: UserIDKeys.imagesArray.rawValue)
    }
    @IBAction func likePressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        imagesArray[currentImageIndex].isLiked = !imagesArray[currentImageIndex].isLiked
        UserDefaults.standard.set(encodable: self.imagesArray, forKey: UserIDKeys.imagesArray.rawValue)
       
    }
    @IBAction func leftButtonPressed(_ sender: UIButton) {
        self.goToPreviousImage()
        self.commentTF.text = imagesArray[currentImageIndex].comment
        self.likeButton.isSelected = imagesArray[currentImageIndex].isLiked
       
    }
    @IBAction func rightButtonPressed(_ sender: UIButton) {
        self.goToNextImage()
        self.commentTF.text = imagesArray[currentImageIndex].comment
        self.likeButton.isSelected = imagesArray[currentImageIndex].isLiked
       
    }
   //MARK: Flow functions
    func downloadSavedInfo() {
        if UserDefaults.standard.value([Image].self, forKey: UserIDKeys.imagesArray.rawValue) != nil {
            self.imagesArray = UserDefaults.standard.value([Image].self, forKey: UserIDKeys.imagesArray.rawValue) ?? []
        }
    }
    func setUI() {
        let bgFrame = backgroundImageView.frame
        self.topImageView.frame = bgFrame

        self.topImageView?.image = self.loadImage(fileName: self.imagesArray[currentImageIndex].name)

    }

    func setCommentTF(){
        commentTF.returnKeyType = .done
        commentTF.autocapitalizationType = .words
        commentTF.autocorrectionType = .no
        commentTF.delegate = self
        commentTF.text = imagesArray[currentImageIndex].comment
    }

    func setLikeButton() {
        
    }

    func loadImage(fileName: String) -> UIImage? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageUrl = documentsDirectory.appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image

        }
        return nil
    }
   
    func goToNextImage() {
        self.topImageView?.frame.origin.x = self.view.frame.width + 5
        //        self.topImageView?.alpha = 1
        self.plusImageIndex()
        self.topImageView?.image = self.loadImage(fileName: self.imagesArray[currentImageIndex].name)

        UIView.animate(withDuration: 0.3) {

            self.topImageView?.frame.origin.x = self.backgroundImageView.frame.origin.x
            //            self.view.layoutIfNeeded()
        } completion: { (_) in
            self.backgroundImageView.image = self.loadImage(fileName: self.imagesArray[self.currentImageIndex].name)
            //            self.view.layoutIfNeeded()
        }

    }
    func goToPreviousImage() {
        self.topImageView?.frame.origin.x = self.view.frame.origin.x - self.backgroundImageView.frame.width
        self.minusImageIndex()
        self.topImageView?.image = self.loadImage(fileName: self.imagesArray[self.currentImageIndex].name)
        //        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {

            self.topImageView?.frame.origin.x =  self.backgroundImageView.frame.origin.x
            self.view.layoutIfNeeded()
        } completion: { (_) in
            self.backgroundImageView.image = self.loadImage(fileName: self.imagesArray[self.currentImageIndex].name)
            //self.view.layoutIfNeeded()
        }


    }

    func plusImageIndex() {
        if self.currentImageIndex == self.imagesArray.count - 1 {
            self.currentImageIndex = 0
        }else{
            self.currentImageIndex += 1
        }
    }

    func minusImageIndex() {
        if self.currentImageIndex <= 0 {
            self.currentImageIndex = self.imagesArray.count - 1
        }else{
            currentImageIndex -= 1
        }
    }
    
    private func registerForKeyboardNotifications() {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        }



        @IBAction private func keyboardWillShow(_ notification: NSNotification) {
            guard let userInfo = notification.userInfo,
                  let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
                  let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
            
            if notification.name == UIResponder.keyboardWillHideNotification {
                scrollViewBottomConstraint.constant = 0
            } else {
                scrollViewBottomConstraint.constant = keyboardScreenEndFrame.height
            }
            
            //view.needsUpdateConstraints()
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        }

}
extension GalleryViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
}
