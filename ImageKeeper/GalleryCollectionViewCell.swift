//
//  GalleryCollectionViewCell.swift
//  ImageKeeper
//
//  Created by Марина Поворотная on 23.08.21.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(with object: Image) {
        imageView.image = self.loadImage(fileName: object.name)
    }
    func loadImage(fileName: String) -> UIImage? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageUrl = documentsDirectory.appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image

        }
        return nil
    }
    
}
