//
//  ViewController.swift
//  ImageKeeper
//
//  Created by Марина Поворотная on 5.05.21.
//

import UIKit
import SwiftyKeychainKit

class ViewController: UIViewController {
    
    //MARK: IBOtlets
    @IBOutlet weak var registrationLabel: UILabel!
    @IBOutlet weak var registrationView: UIView!
    @IBOutlet weak var logInView: UIView!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var registrationPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    
    //MARK: var/lets
    var password = String()
    var passwordIsConfirmed = false
    let keychain = Keychain(service: "com.swifty.keychain")
    let passwordKey = KeychainKey<String>(key: "password")
    
    
    //MARK: Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setAllTF()
        if UserDefaults.wasAppLaunched() {
            self.registrationView.removeFromSuperview()
            self.registrationLabel.removeFromSuperview()
            self.registrationButton.removeFromSuperview()
        }else{
            self.logInButton.removeFromSuperview()
            self.logInView.removeFromSuperview()
        }
        
    }
    //MARK: IBActions
    
    @IBAction func registrationPasswordInputted(_ sender: UITextField) {
        guard let text = sender.text else {
            return
        }
        self.password = text
    }
    
    @IBAction func confirmPasswordInputted(_ sender: UITextField) {
        guard let text = sender.text else {
            return
        }
        if text != self.password {
            self.createAlert("Password is not equal")
        }else{
            self.passwordIsConfirmed = true
        }
    }
    
    @IBAction func passwordInputted(_ sender: UITextField) {
        guard let text = sender.text else {
            return
        }
        self.password = text
    }
    @IBAction func logInButtonPressed(_ sender: UIButton) {
        guard let savedPassword = try? keychain.get(passwordKey) else {
            return
        }
        print(savedPassword)
        if self.password == savedPassword {
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainScreenViewController") as? MainScreenViewController else {
                return
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            self.createAlert("Password is not correct")
        }
    }
    @IBAction func registrationButtonPressed(_ sender: UIButton) {
        if passwordIsConfirmed {
            try? keychain.set(password, for : passwordKey)
            UserDefaults.setAppWasLaunched()
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainScreenViewController") as? MainScreenViewController else {
                return
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            self.createAlert("Password is not equal")
        }
    }
    
    //MARK: Flow functions
    
    func setTF(_ textField: UITextField){
        textField.returnKeyType = .done
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.delegate = self
    }
    
    func setAllTF() {
        self.setTF(registrationPasswordTF)
        self.setTF(passwordTF)
    }
    
    func createAlert(_ text: String) {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
//MARK: Extensions
extension ViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}

